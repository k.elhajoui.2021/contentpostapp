import webapp

class contentPostApp(webapp.webApp):
    """Web application for managing content with support for POST requests."""

    # Dictionary to store content
    content = {'/': 'Root page',
               '/page': 'A page'}

    def parse(self, request):
        """Parse the request and return the resource name."""
        return request.split(' ', 2)[1]

    def process(self, resourceName):
        """Process the request."""
        if resourceName in self.content:
            httpCode = "200 OK"
            htmlBody = "<html><body>" + self.content[resourceName] + "</body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "Not Found"

        # Add form for updating content
        htmlBody += """
            <form method="post" action="/">
                <label for="resource">Resource:</label><br>
                <input type="text" id="resource" name="resource"><br>
                <label for="content">Content:</label><br>
                <textarea id="content" name="content"></textarea><br>
                <input type="submit" value="Submit">
            </form>
        """
        return (httpCode, htmlBody)

    def process_post(self, request):
        """Process POST request to update content."""
        body = request.split('\r\n')[-1]
        parts = body.split('&')
        resource = parts[0].split('=')[1]
        content = parts[1].split('=')[1]
        self.content[resource] = content
        return ("200 OK", "<html><body>Content updated successfully!</body></html>")

if __name__ == "__main__":
    testWebApp = contentPostApp("localhost", 1234)
